<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="landingstyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
   <%  
       response.setHeader("Cache-Control","no-cache, no-store,must-revalidate");
       if(session.getAttribute("name")==null) {
    	   response.sendRedirect("index.jsp");
       }
   %>
   
    <div class="container">
    <br> <br> <br>
      <h2 style="font-size:1.5rem"> Welcome To Landing Page, ${name} </h2>
      <p style="font-size: 1rem;margin-left:10%;margin-right:7%;"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
      <form action="logout">
      <input id="btn" style="width:20%" type="submit" value="Logout">
      </form>
    </div>
</body>
</html>