<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="jakarta.servlet.http.HttpServletRequest" import="jakarta.servlet.http.HttpSession"
    import="javax.swing.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
   <div class="loginBox"> <img class="user" src="https://i.ibb.co/yVGxFPR/2.png" height="100px" width="100px">
    <h3>Sign Up here</h3>
    <form action="signup" method="post">
        <div class="inputBox"> 
        <input  type="text" name="Name" autocomplete="off"  placeholder="Name"> 
        <input  type="email" name="Email" autocomplete="off" pattern="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$" placeholder="Email"> 
        <input  type="number" name="Mobile" autocomplete="off" pattern="\\A[0-9]{10}\\z" placeholder="Mobile number">
        <input  type="password" name="Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,12}$" placeholder="Password"> 
        </div>                                 
        <input type="submit" name="" value="Submit">
    </form> 
   </div>

</body>
</html>