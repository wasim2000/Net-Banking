package org.login.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.login.resource.*;

import java.lang.Math;

@WebServlet("/mail")
public class MailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String to=(String) request.getParameter("Email");
		String number=(String) request.getParameter("Mobile");
		LoginDao dao=new LoginDao();
        if(to.trim().equals("")) {
			
			String name=dao.getName();
			int otp=(int)(Math.random()*900000)+100000;
			String message="Hello, Mr."+name+", You requested to change your password.\nYour OTP value to change password is: "+otp;
			String apikey="KdMARu5tZ0fmSQUJzFcEoOTlx39yWDihGHaL4jN6kbn18we7IplnwKUR4vX6Os1BoL2zdQe579tCI8hj";
			if(TextMessage.sendOTP(message,number,apikey)) {
				System.out.println("Message sent successfully");
				request.getSession().setAttribute("OTP", otp);
				request.getSession().setAttribute("Number", number);
				response.sendRedirect("check.jsp");
			}
			else {
				System.out.println("Message Failed");
				response.sendRedirect("Update.jsp");
			}
			
		}
        else {
			
			String name=dao.getName();
			int otp=(int)(Math.random()*900000)+100000;
			String subject="Forgot Password Verification";
			String message="Hello, Mr."+name+", You requested to change your password.\nYour OTP value to change password is: "+otp;
			Mail mail=new Mail();
			boolean mail_sent=mail.SendMail(to, subject, message);
			if(mail_sent) {
				System.out.println("Mail sent successfully");
				request.getSession().setAttribute("OTP", otp);
				request.getSession().setAttribute("Email", to);
				response.sendRedirect("check.jsp");
			}
			else {
				System.out.println("Mail Failed");
				response.sendRedirect("Update.jsp");
			}
        }
	}

}
