<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome</title>
 <link rel="stylesheet" href="CSS Folder/style1.css">
</head>
<body>
   <%  
       response.setHeader("Cache-Control","no-cache, no-store,must-revalidate");
       if(session.getAttribute("name")==null) {
    	   response.sendRedirect("login.jsp");
       }
   %>
   
  <div class="container">

    <header>

      <nav class="navbar">
         <img src="images/logo.svg">
        <button class="navbar-toggle-btn">
          <span class="one"></span>
          <span class="two"></span>
          <span class="three"></span>
        </button>

        <ul class="navbar-nav">

          <li><a href="#home">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="#">Contact</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Careers</a></li>

        </ul>

        

      </nav>

    </header>

    <main>

   
      <section class="home" id="home">

        <div class="home-img-box">
          <img src="./images/image-mockups.png" alt="Image for Easybank Banner" class="home-img" width="100%">
        </div>

        <div class="wrapper">
          <h1 class="home-title">Next generation digital banking</h1>

          <p class="home-text">
            Take your financial life online. Your Easybank account will be a one-stop-shop
            for spending, saving, budgeting, investing, and much more.
          </p>

        
        </div>

      </section>


      <section class="service">

        <h2 class="section-title">Why choose Easybank?</h2>

        <p class="section-text">
          We leverage Open Banking to turn your bank account into your financial hub. Control
          your finances like never before.
        </p>


        <div class="service-card-group">

          <div class="service-card">
           <img onclick="location.href='withdraw.jsp';" src="https://img.icons8.com/external-itim2101-lineal-color-itim2101/64/000000/external-cash-withdrawal-finance-itim2101-lineal-color-itim2101.png"/>

            <h3 onclick="location.href='withdraw.jsp';" class="card-title">WithDraw Amount</h3>

            <p class="card-text">
              Our modern web and mobile applications allow you to keep track of your finances
              wherever you are in the world.
            </p>
          </div>

          <div class="service-card">
            <img onclick="location.href='deposit.jsp';" src="https://img.icons8.com/external-itim2101-lineal-color-itim2101/64/000000/external-deposit-finance-itim2101-lineal-color-itim2101.png"/>

            <h3 onclick="location.href='deposit.jsp';" class="card-title">Deposit Amount</h3>

            <p class="card-text">
              See exactly where your money goes each month. Receive notifications when you’re
              close to hitting your limits.
            </p>
          </div>

          <div class="service-card">
           <img onclick="location.href='transfer.jsp';" src="https://img.icons8.com/external-wanicon-lineal-color-wanicon/64/000000/external-transfer-currency-wanicon-lineal-color-wanicon.png"/>

            <h3 onclick="location.href='transfer.jsp';" class="card-title">Money Transfer</h3>

            <p class="card-text">
              We don’t do branches. Open your account in minutes online and start taking control
              of your finances right away.
            </p>
          </div>

          <div class="service-card">
            <img src="https://img.icons8.com/fluency/48/000000/online-support.png"/>

            <h3 class="card-title">Online Support</h3>

            <p class="card-text">
              Manage your savings, investments, pension, and much more from one account. Tracking
              your money has never been easier.
            </p>
          </div>
          
          <div class="service-card">
            <img onclick="location.href='balance.jsp';" src="https://img.icons8.com/doodle/48/000000/money.png"/>

            <h3 onclick="location.href='balance.jsp';" class="card-title">Check Balance</h3>

            <p class="card-text">
              Manage your savings, investments, pension, and much more from one account. Tracking
              your money has never been easier.
            </p>
          </div>
          
          <div class="service-card">
            <img src="https://img.icons8.com/external-sbts2018-lineal-color-sbts2018/58/000000/external-most-recent-social-media-sbts2018-lineal-color-sbts2018-1.png"/>

            <h3 class="card-title">Recent Transaction</h3>

            <p class="card-text">
              Manage your savings, investments, pension, and much more from one account. Tracking
              your money has never been easier.
            </p>
          </div>
           <div class="service-card">
            <img onclick="location.href='logout.jsp';" src="https://img.icons8.com/external-sbts2018-lineal-color-sbts2018/58/000000/external-logout-social-media-sbts2018-lineal-color-sbts2018.png"/>

            <h3 onclick="location.href='logout.jsp';" class="card-title">Logout</h3>

            <p class="card-text">
              Manage your savings, investments, pension, and much more from one account. Tracking
              your money has never been easier.
            </p>
          </div>
           <div class="service-card">
            <img onclick="location.href='delete.jsp';" src="https://img.icons8.com/external-xnimrodx-lineal-color-xnimrodx/64/000000/external-delete-calendar-xnimrodx-lineal-color-xnimrodx.png"/>

            <h3 onclick="location.href='delete.jsp';"  class="card-title">Delete Account</h3>

            <p class="card-text">
              Manage your savings, investments, pension, and much more from one account. Tracking
              your money has never been easier.
            </p>
          </div>

        </div>
      </section>
      </main>
      </div>
</body>
</html>