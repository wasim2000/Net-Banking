	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login Form</title>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600&display=swap" rel="stylesheet">
<link rel="stylesheet" href="CSS Folder/style.css">
</head>
<body>
  <div class="background">
        <div class="shape"></div>
        <div class="shape"></div>
    </div>
    <form class="form" action="login" method="post">
        <h3>EasyBank Login Here</h3>
        <label for="username">Account Number</label>
        <input type="number" placeholder="Account No" name="Account" autocomplete="off" id="username">
        <label for="mobile">Email</label>
        <input type="text" placeholder="Email" name="Email" autocomplete="off" id="mobile">
        <label for="password">Password</label>
        <input type="password" placeholder="Password" name="Pass" id="password">

        <button><i style="color:black" class="fas fa-sign-in-alt"></i> Log In</button>
        <div class="social">
          <div onclick="location.href='signup.jsp';" style="cursor: pointer;" class="go"><i class="fas fa-user-alt"></i> Create Account</div>
          <div onclick="location.href='verify.jsp';" class="fb"><i class="far fa-question-circle"></i>Forgot Password?</div>
        </div>
    </form>
</body>
</html>