<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SignUp Form</title>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600&display=swap" rel="stylesheet">
<link rel="stylesheet" href="CSS Folder/style.css">
</head>
<body>
<div class="background">
        <div style="left: -120px;top: -210px;" class="shape"></div>
        <div style="right:-100px;bottom:-350px;" class="shape"></div>
    </div>
    <form name="myForm" onsubmit="return validateForm()" style="height:920px;top:54%" action="signup" method="post">
        <h3>Create Bank Account</h3>

        <label>Name</label>
        <input type="text" placeholder="Name" autocomplete="off" name="Name">
        <label>Email</label>
        <input type="email" placeholder="Email" autocomplete="off"  name="Email">
        <label>Mobile</label>
        <input type="text" placeholder="Mobile" pattern="[1-9]{1}[0-9]{9}" autocomplete="off" name="Mobile">
         <label>DateOfBirth</label>
        <input type="date" name="Date">
        <label>Amount</label>
        <input type="number" placeholder="Initial Amount" name="Amount">
        <label> Select Documents </label>  
        <select name="Proof" style="color:black">
          <option style="color:black" value =""selected>Select Proof</option>
          <option style="color:black" value = "Aadhar">Aadhar Card</option> 
          <option style="color:black" value = "Pan">Pan Card</option> 
          <option style="color:black" value = "Passport">Passport</option> 
          <option style="color:black" value = "Ration">Ration Card</option> 
        </select>
        <label>Password</label>
        <input type="password" placeholder="Password"  name="Password">
        <label>Confirm Password</label>
        <input type="password" placeholder="Confirm Password"  name="Confirm">
        <button style="margin-top:100px;margin-bottom:40px"><i style="color:black;" class="fas fa-user-alt"></i> Sign Up</button>
        
    </form>
    <script>
    function validateForm() {
    	  var x = document.forms["myForm"]["Name"].value;
    	  var a = document.forms["myForm"]["Email"].value;
    	  var b = document.forms["myForm"]["Mobile"].value;
    	  var c = document.forms["myForm"]["Amount"].value;
    	  var d = document.forms["myForm"]["Password"].value;
    	  var e = document.forms["myForm"]["Confirm"].value;
    	  if (x == "" || a=="" || b=="" || d=="" || e=="") {
    	    alert("Fields must be filled out");
    	    return false;
    	  }
    	  if(c<500)  {
    		alert("Minimum amount should be 500");
      	    return false;
    	  }
    	}
    </script>
</body>
</html>