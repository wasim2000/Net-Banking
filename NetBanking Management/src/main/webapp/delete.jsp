<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>delete form</title>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600&display=swap" rel="stylesheet">
<link rel="stylesheet" href="CSS Folder/style.css">
</head>
<body>
<%  
       response.setHeader("Cache-Control","no-cache, no-store,must-revalidate");
       if(session.getAttribute("name")==null) {
    	   response.sendRedirect("login.jsp");
       }
   %>
  <div class="background">
        <div style="left: -120px;top: -210px;" class="shape"></div>
        <div style="right:-115px;bottom:-300px;" class="shape"></div>
    </div>
    <form style="height:775px;top:54%" action="delete" method="post">
        <h3>Delete Bank Account</h3>
        <label>Account Number</label>
        <input type="number" placeholder="Account Number" autocomplete="off" name="acc_no">        
        <label>Email</label>
        <input type="email" placeholder="Email" autocomplete="off" name="Email">
        <label>Mobile</label>
        <input type="number" placeholder="Mobile" autocomplete="off" name="Mobile">
        <label>ATM PIN</label>
        <input type="number" placeholder="ATM Pin" autocomplete="off" name="pin">
        <label>CVV</label>
        <input type="number" autocomplete="off" placeholder="CVV" name="cvv">  
        <label>Password</label>
        <input type="password" placeholder="Password" name="Password">
        <button style="margin-top:65px;margin-bottom:40px"><i style="color:black;" class="fas fa-user-alt"></i> Delete </button>
        
    </form>
</body>
</html>