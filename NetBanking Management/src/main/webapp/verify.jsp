<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OTP Verification</title>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600&display=swap" rel="stylesheet">
<link rel="stylesheet" href="CSS Folder/style.css">
</head>
<body>
  <div class="background">
        <div class="shape"></div>
        <div class="shape"></div>
    </div>
    <form action="verify" method="post">
        <h3>Please Verify</h3>
        <label>Enter Mail</label>
        <input type="email" placeholder="Email" name="email">
        <label>Password</label>
        <input type="password" placeholder="Set Password" name="pass">
        <label>Confirm Password</label>
        <input type="password" placeholder="Confirm Password" name="newpass">
              
        <button><i style="color:black" class="fas fa-download"></i></i> Submit</button>
        
    </form>
</body>
</html>