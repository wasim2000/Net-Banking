package org.bank.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.bank.demo.resource.LoginDao;

@WebServlet("/balance")
public class BalanceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		LoginDao dao=new LoginDao();
		int acc_no=Integer.valueOf(request.getParameter("acc_no"));
		int pin=Integer.valueOf(request.getParameter("pin"));
		if(dao.balance(acc_no, pin)) {
			JOptionPane.showMessageDialog(null, "HELLO, YOUR BALANCE AMOUNT IS: "+dao.getAmount());
			response.sendRedirect("welcome.jsp");
		}
		else {
			JOptionPane.showMessageDialog(null, "YOUR INPUT IS WRONG");
			response.sendRedirect("balance.jsp");
		}
	}

}
