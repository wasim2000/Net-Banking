package org.bank.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.bank.demo.resource.LoginDao;
import org.bank.demo.resource.Mail;
import org.bank.demo.resource.TextMessage;

@WebServlet("/delete")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int acc_no=Integer.valueOf(request.getParameter("acc_no"));
		String email=request.getParameter("Email");
		String number=request.getParameter("Mobile");
		int pin=Integer.valueOf(request.getParameter("pin"));
		int cvv=Integer.valueOf(request.getParameter("cvv"));
		String pass=request.getParameter("Password");
		int otp=100000+(int)Math.floor(Math.random()*900000);
		int otp1=100000+(int)Math.floor(Math.random()*900000);
		String message="We are sorry to hear that you are closing your account\nYour OTP to delete your account is:"+otp;
		TextMessage.sendOTP(message, number);
		String subject="EasyBank Private Limited";
		String msg_body="Hello user,we feel sorry for you are deleting account in Easybank.\nYour Otp to delete account is:"+otp1;
		Mail mail=new Mail();
		mail.SendMail(email, subject, msg_body);
		int userotp=Integer.valueOf(JOptionPane.showInputDialog("Hello,Please enter your Mobile otp"));
		int userotp1=Integer.valueOf(JOptionPane.showInputDialog("Hello,Please enter your Mail otp"));
		if(otp==userotp && otp1==userotp1) {
			 LoginDao dao=new LoginDao();
			 if(dao.delete(acc_no, email, number, pin, cvv, pass)) {
				 HttpSession session=request.getSession();
				 session.removeAttribute("name");
				 session.invalidate();
				 JOptionPane.showMessageDialog(null, "DEAR USER,YOUR ACCOUNT DELETED SUCESSFULLY");
				 response.sendRedirect("signup.jsp");
			 }
			 else {
				 JOptionPane.showMessageDialog(null, "YOUR INPUT IS INVALID,PLEASE TRY AGAIN LATER");
				 response.sendRedirect("delete.jsp");
			 }
		}
		else {
			JOptionPane.showMessageDialog(null, "YOUR OTP IS NOT CORRECT, PLEASE TRY AGAIN" );
			response.sendRedirect("delete.jsp");
		}
	}

}
