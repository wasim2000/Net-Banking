package org.bank.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.bank.demo.resource.LoginDao;
import org.bank.demo.resource.Mail;

@WebServlet("/transfer")
public class TransferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int myacc=Integer.valueOf(request.getParameter("acc_no"));
		int receiver=Integer.valueOf(request.getParameter("receiver"));
		int pin=Integer.valueOf(request.getParameter("pin"));
		int cvv=Integer.valueOf(request.getParameter("cvv"));
		int amount=Integer.valueOf(request.getParameter("amount"));
		int encrypt=myacc%10;
		int encrypt1=receiver%10;
		LoginDao dao=new LoginDao();
		if(!dao.check("", request.getParameter("mymobile")) && !dao.check("", request.getParameter("oppmobile")) ) {
			 if(dao.transfer(myacc, receiver,pin,cvv, amount)) {
				 Mail mail=new Mail();
				 String subject="EasyBank Private Limited";
			     String message="Hello user, Rs."+amount+" has been depited from your acount number:****"+encrypt+"\nTotal Balance is:"+dao.getAmount()+"\nThankYou, Have a nice day";
                 mail.SendMail(dao.getEmail(), subject, message);
			     message="Hello user, Rs."+amount+" has been credited from your acount number:****"+encrypt1+"\nTotal Balance is:"+dao.getoppAmount()+"\nThankYou, Have a nice day";
                 mail.SendMail(dao.getoppEmail(), subject, message);
                 JOptionPane.showMessageDialog(null, "MONEY TRANSFERED SUCCESSFULLY");
                 response.sendRedirect("welcome.jsp");
			 }
			 else {
				 JOptionPane.showMessageDialog(null, "MONEY TRANSACTION FAILED, PLESE CHECK YOUR INPUT");
                 response.sendRedirect("transfer.jsp");
			 }
		}
		else {
			JOptionPane.showMessageDialog(null, "WE COULDN'T FIND YOUR INPUT. PLEASE TRY AGAIN");
			response.sendRedirect("transfer.jsp");
		}
	}

}
