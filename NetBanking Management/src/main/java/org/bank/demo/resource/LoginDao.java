package org.bank.demo.resource;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class LoginDao {
	Connection con = null;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    private String name,acc_no,atm_expDate,Email_id,oppEmail;
    private int atmpin,cvv,Amount,oppAmount;
    boolean flag;
      
    public LoginDao(){
    	 try{
             Class.forName("com.mysql.cj.jdbc.Driver");
             con = DriverManager.getConnection(
                     "jdbc:mysql://localhost:3306/Banking","root","root");

         } catch (Exception e) {
             e.printStackTrace();
         }
     }
     public boolean service(int Account_Number,String email,String pass)  {
 		try {
 	     PreparedStatement stmt=con.prepareStatement("select * from Persons  where Account_Number = ? and Email=? and Password=?");
 	     stmt.setInt(1, Account_Number);
 	     stmt.setString(2, email);
 	     stmt.setString(3, pass);
 	     ResultSet rs=stmt.executeQuery();
 	     while(rs.next()) {
 	    	 name=rs.getString(2);
 	    	 con.close();
 	    	 return true;
 	     }
 		} catch(Exception se) {
 			System.out.println(se);
 		} 
 		return false;
     }
     public boolean createNew(String name,String email,String mobile,Date date,int amount,int proof,String pass,int pin,int cvv) {
    	 try {
     	    PreparedStatement stmt=con.prepareStatement("INSERT INTO Persons(Name,Email,Mobile,DOB,Amount,ProofOfIdentity,Password,ATMPin,ATM_ExpDate,CVV) values(?,?,?,?,?,?,?,?,?,?)");
    	    stmt.setString(1,name);
    	    stmt.setString(2,email);
    	    stmt.setString(3,mobile);
    	    stmt.setDate(4,date);
    	    stmt.setInt(5,amount);
    	    stmt.setInt(6,proof);
    	    stmt.setString(7,pass);
    	    stmt.setInt(8,pin);
    	    stmt.setString(9,"12/24");
    	    stmt.setInt(10, cvv);
    	    System.out.println("hello i am here");
    	    int rs=stmt.executeUpdate();
    	    System.out.println(rs);
    	    if(rs==0 ||rs==-1) {
    	    	return false;
    	    }
    	 } catch(Exception e) {
    		 e.printStackTrace();
    	 }
    	 return true;
     }
     public boolean check(String email,String number)  {
    	 try {
    		 PreparedStatement stmt=con.prepareStatement("select * from Persons where Email=? or Mobile=?");
    		 stmt.setString(1,email);
    		 stmt.setString(2, number);
    		 ResultSet rs=stmt.executeQuery();
    		 while(rs.next()) {
    			 name=rs.getString(2);
    			 acc_no=rs.getString(1);
    			 Email_id=rs.getString(3);
     	    	 atm_expDate=rs.getString(10);
     	    	 atmpin=rs.getInt(9);
     	    	 cvv=rs.getInt(11);
    			 return false;
    		 }
    	 } catch(Exception e) {
    		 e.printStackTrace();
    	 }
    	 return true;
     }
     public boolean update(String email,String password) {
    	 try {
    		 PreparedStatement stmt=con.prepareStatement("UPDATE Persons SET Password=? WHERE Email=?");
    		 stmt.setString(1, password);
    		 stmt.setString(2, email);
    		 if(stmt.execute()) {
    			 return false;
    		 }
    	 }catch(Exception e) {
    		 e.printStackTrace();
    	 }
    	 return true;
     }
     public boolean withdraw(int acc_no,int pin,String expdate,int cvv,int amount) {
    	 try {
    		 PreparedStatement stmt=con.prepareStatement("select Amount,Email from Persons where Account_Number=? and ATMPin=? and ATM_ExpDate=? and CVV=?");
    		 stmt.setInt(1,acc_no);
    		 stmt.setInt(2, pin);
    		 stmt.setString(3, expdate);
    		 stmt.setInt(4, cvv);
    		 ResultSet rs=stmt.executeQuery();
    		 while(rs.next()) {
    			 Amount=rs.getInt(1);
    			 Email_id=rs.getString(2);
    		 }
    		 if(amount>0 && amount<Amount) {
    			 Amount=Amount-amount;
    		 } else {return false;}
    		 PreparedStatement stmt1=con.prepareStatement("UPDATE Persons SET Amount=? where Account_Number=?");
    		 stmt1.setInt(1,Amount);
    		 stmt1.setInt(2, acc_no);
    		 if(stmt1.execute()) {
    			 return false;
    		 }
    	 } catch(Exception e) {
    		 e.printStackTrace();
    	 }
    	     return true;
     }
     public boolean deposit(int acc_no,int amount) {
    	 try {
    		 PreparedStatement stmt=con.prepareStatement("select Amount,Email from Persons where Account_Number=?");
    		 stmt.setInt(1, acc_no);
    		 ResultSet rs=stmt.executeQuery();
    		 while(rs.next()) {
    			 Amount=rs.getInt(1);
    			 Email_id=rs.getString(2);
    		 }
    		 if(amount>0) {
    			 Amount=Amount+amount;
    		 } else {return false;}
    		 PreparedStatement stmt1=con.prepareStatement("UPDATE Persons SET Amount=? where Account_Number=?");
    		 stmt1.setInt(1,Amount);
    		 stmt1.setInt(2, acc_no);
    		 if(stmt1.execute()) {
    			 return false;
    		 }
    	 }catch(Exception e) {
    		 e.printStackTrace();
    	 }
	     return true;
     }
     public boolean transfer(int sender,int receiver,int pin,int cvv,int amount) {
    	 try {
    		 PreparedStatement stmt=con.prepareStatement("select Amount,Email from Persons where Account_Number=? and  ATMPin=? and CVV=?");
    		 stmt.setInt(1, sender);
    		 stmt.setInt(2, pin);
    		 stmt.setInt(3, cvv);
    		 ResultSet rs=stmt.executeQuery();
    		 while(rs.next()) {
    			 Amount=rs.getInt(1);
    			 Email_id=rs.getString(2);
    		 }
    		 PreparedStatement stmt1=con.prepareStatement("select Amount,Email from Persons where Account_Number=?");
    		 stmt1.setInt(1, receiver);
    		 ResultSet rs1=stmt1.executeQuery();
    		 while(rs1.next()) {
    			 oppAmount=rs1.getInt(1);
    			 oppEmail=rs1.getString(2);
    		 }
    		 Amount=Amount-amount;
    		 oppAmount+=amount;
    		 PreparedStatement stmt2=con.prepareStatement("UPDATE Persons SET Amount=? where Account_Number=?");
    		 stmt2.setInt(1, Amount);
    		 stmt2.setInt(2, sender);
    		 stmt2.execute();
    		 PreparedStatement stmt3=con.prepareStatement("UPDATE Persons SET Amount=? where Account_Number=?");
    		 stmt3.setInt(1, oppAmount);
    		 stmt3.setInt(2, receiver);
    		 if(stmt3.execute()) {
    			 return false;
    		 }
    	 }catch(Exception e) {
    		 e.printStackTrace();
    	 }
    	 return true;
     }
     public boolean balance(int acc_number,int pin) {
    	 try {
    		 PreparedStatement stmt=con.prepareStatement("select Amount from Persons where Account_Number=? and ATMPin=?");
             stmt.setInt(1, acc_number);
             stmt.setInt(2, pin);
             ResultSet rs=stmt.executeQuery();
             while(rs.next()) {
            	 Amount=rs.getInt(1);
            	 return true;
             }
    	 }catch(Exception e) {
    		 e.printStackTrace();
    	 }
    	 return false;
     }
     public boolean delete(int acc_no,String email,String number,int pin,int cvv,String password) {
    	 try {
    		 PreparedStatement stmt=con.prepareStatement("delete from Persons where Account_Number=? and Email=? and Mobile=? and  ATMPin=? and CVV=? and Password=?");
             stmt.setInt(1, acc_no);
             stmt.setString(2, email);
             stmt.setString(3, number);
             stmt.setInt(4, pin);
             stmt.setInt(5, cvv);
             stmt.setString(6, password);
             if(stmt.execute()) {
            	 return false;
             }
    	 }catch(Exception e) {
    		 e.printStackTrace(); 
    	 }
    	 return true;
     }
     public String getName() {
    	 return name;
     }
     public String getAcc_no() {
    	 return acc_no;
     }
     public String getAcc_ExpDate() {
    	 return atm_expDate;
     }
     public String getEmail() {
    	 return Email_id;
     }
     public String getoppEmail() {
    	 return oppEmail;
     }
     public int getAmount() {
    	 return Amount;
     }
     public int getAtmPin() {
    	 return atmpin;
     }
     public int getCVV() {
    	 return cvv;
     }
     public int getoppAmount() {
    	 return oppAmount;
     }
}
