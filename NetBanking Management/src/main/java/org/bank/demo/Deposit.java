package org.bank.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.bank.demo.resource.LoginDao;
import org.bank.demo.resource.Mail;

@WebServlet("/deposit")
public class Deposit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int acc_no=Integer.valueOf(request.getParameter("acc_no"));
		int confirm=Integer.valueOf(request.getParameter("confirm"));
		String name=request.getParameter("name");
		int amount=Integer.valueOf(request.getParameter("amount"));
		LoginDao dao=new LoginDao();
			int encrypt=acc_no%10;
			if(acc_no==confirm) {
				if(dao.deposit(acc_no, amount)) {
					JOptionPane.showMessageDialog(null, "MR."+name+" DEPOSITED AMOUNT SUCESSFULLY.THANKYOU FOR USING OUR SERVICE");			
					Mail mail=new Mail();
					String subject="EasyBank Private Limited";
					String message="Hello user, Rs."+amount+" has been credited from your acount number:****"+encrypt+"\nTotal Balance is:"+dao.getAmount()+"\nThankYou, Have a nice day";
					mail.SendMail(dao.getEmail(), subject, message);
					response.sendRedirect("welcome.jsp");
				}
				else {
					JOptionPane.showMessageDialog(null,"SOMETHING WRONG HAPPENED, PLEASE TRY AGAIN");
					response.sendRedirect("withdraw.jsp");
				}
			} else {
				JOptionPane.showMessageDialog(null,"YOUR ACCOUNT NUMBER AND CONFIRM ACCOUNT ARE NOT SAME");
				response.sendRedirect("withdraw.jsp");
			}

	}

}
