package org.bank.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.bank.demo.resource.LoginDao;
import org.bank.demo.resource.Mail;

@WebServlet("/withdraw")
public class Withdraw extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		LoginDao dao=new LoginDao();
		int acc_no=Integer.valueOf(request.getParameter("acc_no"));
		int ATM=Integer.valueOf(request.getParameter("pin"));
		String exp_date=request.getParameter("expdate");
		String mobile=request.getParameter("mobile");
		int cvv=Integer.valueOf(request.getParameter("cvv"));
		int amount=Integer.valueOf(request.getParameter("amount"));
		int encrypt=acc_no%100;
		Mail mail=new Mail();
		String subject="EasyBank Private Limited";
		int otp=100000+(int)Math.floor(Math.random()*900000);
		String message="Hello user, Your OTP to withdraw amount is:"+otp;
		dao.check("", mobile);
		mail.SendMail(dao.getEmail(), subject, message);
		int userotp=Integer.valueOf(JOptionPane.showInputDialog("Please enter the OTP sent to your mail"));
		if(otp==userotp) {
			if(dao.withdraw(acc_no, ATM, exp_date, cvv, amount)) {
				JOptionPane.showMessageDialog(null, "YOUR AMOUNT IS WITHDRAWN SUCESSFULLY.THANKYOU FOR USING OUR SERVICE");
				message="Hello user, Rs."+amount+" has been depited from your acount number:***"+encrypt+"\nTotal Balance is:"+dao.getAmount()+"\nThankYou, Have a nice day";
				mail.SendMail(dao.getEmail(), subject, message);
				response.sendRedirect("welcome.jsp");
			}
			else {
				JOptionPane.showMessageDialog(null,"SOMETHING WRONG HAPPENED, PLEASE TRY AGAIN");
				response.sendRedirect("withdraw.jsp");
			}
	    } else {
	    	JOptionPane.showMessageDialog(null,"YOUR OTP IS NOT CORRECT, PLEASE TRY AGAIN");
			response.sendRedirect("withdraw.jsp");
	    }	
	}

}
