package org.bank.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.swing.*;

import org.bank.demo.resource.LoginDao;
import org.bank.demo.resource.TextMessage;
import org.bank.demo.resource.Mail;

@WebServlet("/signup")
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		LoginDao dao=new LoginDao();
		String name=request.getParameter("Name");
		String number=request.getParameter("Mobile");
		String email=request.getParameter("Email");	
		int otp=100000+(int)Math.floor(Math.random()*900000);
		String message="Welcome To EasyBank\nYour OTP to create your account is:"+otp;
		String proof=request.getParameter("Proof");
		int option=0;
		switch(proof){
		  case "Aadhar": option=1; break;
		  case "Pan":option=2;break;
		  case "Passport": option=3;break;
		  case "Ration": option=4;break;
		  default: option=0;;
		}
		if(option!=0 && dao.check(email, number)) {
		TextMessage.sendOTP(message, number);
		int userotp=Integer.valueOf(JOptionPane.showInputDialog("Hello "+name+",Please enter your Mobile otp"));
		if(otp==userotp) {	
			String date=request.getParameter("Date");
			String amount=request.getParameter("Amount");
			int amt=Integer.valueOf(amount);
			String pass=request.getParameter("Password");
			String confirm=request.getParameter("Confirm");
			try {
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date udob=sdf.parse(date);
				long ms=udob.getTime();
				java.sql.Date sqdob=new java.sql.Date(ms); 
				int pin=1000+(int)Math.floor(Math.random()*9000);
				int cvv=100+(int)Math.floor(Math.random()*500);
				if(option!=0 && pass.equals(confirm)) {
					if(dao.createNew(name, email, number, sqdob, amt, option, pass, pin, cvv) ) {
						JOptionPane.showMessageDialog(null, "Your Account created Successfully,Your Account_Details will be sent to your Mail");
						dao.check(email, number);
						String acc_no=dao.getAcc_no();
						String atm_expDate=dao.getAcc_ExpDate();
						int atmpin=dao.getAtmPin();
						int CVV=dao.getCVV();
						String msg_body="Hello "+name+",ThankYou for creating account in Easybank.\nYour account Details is:\n1)Account Number-"+acc_no+"\n2)ATM pin-"+atmpin+"\n3)ATM Expiry Date-"+atm_expDate+"\n4)CVV-"+CVV+"\nThankYou,Have A Nice Day";
						String subject="EasyBank Private Limited";
						Mail mail=new Mail();
						mail.SendMail(email, subject, msg_body);
						response.sendRedirect("login.jsp");
					}
					else {
						JOptionPane.showMessageDialog(null, "Failed to enter data");
						response.sendRedirect("signup.jsp");
					}
				}
				else {
			        JOptionPane.showMessageDialog(null, "Your Password and Confirm Password is mismatch");
					response.sendRedirect("signup.jsp");
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  } else {
		  JOptionPane.showMessageDialog(null, "YOUR OTP IS NOT CORRECT, PLEASE TRY AGAIN");
		  response.sendRedirect("signup.jsp");
	  }
	} else {
		if(option==0) {JOptionPane.showMessageDialog(null, "YOU SHOULD SUBMIT DOCUMENTS");response.sendRedirect("signup.jsp");}
		else {JOptionPane.showMessageDialog(null,"EMAIL OR MOBILE ALREADY EXISTS");response.sendRedirect("signup.jsp");}
	}
 }

}
