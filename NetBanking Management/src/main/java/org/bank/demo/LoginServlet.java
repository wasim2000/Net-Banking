package org.bank.demo;

import jakarta.servlet.ServletException;
import javax.swing.*;

import org.bank.demo.resource.LoginDao;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JFrame f=new JFrame();
	    String Account=request.getParameter("Account");
	    int Acc_No=Integer.valueOf(Account);
	    String email=request.getParameter("Email");
	    String pass=request.getParameter("Pass");
	    LoginDao dao=new LoginDao();
	    HttpSession session=request.getSession();
	    if(dao.service(Acc_No, email, pass)) {
	    	String name=dao.getName();
			session.setAttribute("name",name);
			response.sendRedirect("welcome.jsp");
	    }
	    else {
	    	JOptionPane.showMessageDialog(f, "Please check your input,No match found for your input");
	    	response.sendRedirect("login.jsp");
	    }
	}

}
