package org.bank.demo;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.bank.demo.resource.LoginDao;
import org.bank.demo.resource.Mail;

@WebServlet("/verify")
public class verifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		LoginDao dao=new LoginDao();
		String email=request.getParameter("email");
		String pass=request.getParameter("pass");
		String newpass=request.getParameter("newpass");
		int otp=100000+(int)Math.floor(Math.random()*900000);
		String subject="EasyBank Private Limited";
		String message="Your OTP to edit value is:"+otp+"\nThankYou, Have a nice day";
		if(pass.equals(newpass)) {
			Mail mail=new Mail();
			mail.SendMail(email, subject, message);
			int userotp=Integer.valueOf(JOptionPane.showInputDialog("HELLO USER, PLEASE ENTER YOUR OTP?"));
			 if(userotp==otp) {
				 if(dao.update(email, pass)) {
					 JOptionPane.showMessageDialog(null, "YOUR PASSWORD SUCCESSFULLY CHANGED");
					 response.sendRedirect("login.jsp");
				 }
				 else {
					 JOptionPane.showMessageDialog(null, "SOMETHING WRONG HAPPENED");
					 response.sendRedirect("verify.jsp");
				 }
			 }
			 else {
				 JOptionPane.showMessageDialog(null, "YOUR OTP IS NOT CORRECT,PLEASE TRY AGAIN");
				 response.sendRedirect("verify.jsp");
			 }
		} 
		else {
			JOptionPane.showMessageDialog(null,"YOUR PASSWORD AND CONFIRM PASSWORD ARE NOT SAME");
			response.sendRedirect("verify.jsp");
		}
		
	}

}
